# TASK MANAGER

SCREEN

https://disk.yandex.ru/d/KBH30dOblWDQZA?w=1

## DEVELOPER INFO

name: Elena Ilina

e-mail: eilina@tsconsulting.com

## SOFTWARE

System: Windows 10

Version JDK: 11.0.8

## HARDWARE

CPU: i7

RAM: 16G

SSD: 512GB

## PROGRAM RUN

```bash
java -jar .\task_manager.jar
```